pushd %~dp0

set PATH=%PATH%;%CD%\tools\minimal_adb_fastboot_1.4.3_portable

fastboot.exe -i 0x9886 format cache
fastboot.exe -i 0x9886 format userdata
fastboot.exe -i 0x9886 reboot

pause
popd