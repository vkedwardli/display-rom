pushd %~dp0

set PATH=%PATH%;%CD%\tools\minimal_adb_fastboot_1.4.3_portable



pause
popd


#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
pushd "${DIR}"

export PATH=$PATH:"${DIR}"/tools/nexus-tools-673c086/bin
chmod +x "${DIR}"/tools/nexus-tools-673c086/bin/*fastboot

OS=$(uname)
ARCH=$(uname -m)

SUDO="sudo "

if [ "$OS" == "Linux" ]; then
	if [ "$ARCH" == "i386" ] || [ "$ARCH" == "i486" ] || [ "$ARCH" == "i586" ] || [ "$ARCH" == "amd64" ] || [ "$ARCH" == "x86_64" ] || [ "$ARCH" == "i686" ]; then # Linux on Intel x86/x86_64 CPU
		FASTBOOT="linux-i386-fastboot"
	elif [ "$ARCH" == "arm" ] || [ "$ARCH" == "armv6l" ] || [ "$ARCH" == "armv7l" ]; then # Linux on ARM CPU
		FASTBOOT="linux-arm-fastboot"
	else
		echo "[WARN] Don't know what your linux platform is, will try i386."
		echo "[WARN] If fastboot fails find a copy that is suitable for you system and ensure it's in the path"
		FASTBOOT="linux-i386-fastboot"
	fi

elif [ "$OS" == "Darwin" ]; then
	FASTBOOT="mac-fastboot"
        SUDO=""
else
	echo "[EROR] Don't have fastboot for your platform."
	echo "[EROR] OS: $OS"
	echo "[EROR] ARCH: $ARCH"
	echo "[EROR] find a copy that is suitable for you system and ensure it's in the path"
	FASTBOOT="fastboot"
fi

$SUDO$FASTBOOT -i 0x9886 format cache
$SUDO$FASTBOOT -i 0x9886 format userdata
$SUDO$FASTBOOT -i 0x9886 reboot

echo "done"
popd
