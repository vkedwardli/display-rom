pushd %~dp0

set PATH=%PATH%;%CD%\tools\minimal_adb_fastboot_1.4.3_portable

fastboot.exe -i 0x9886 flash boot boot.img
fastboot.exe -i 0x9886 flash recovery recovery.img
fastboot.exe -i 0x9886 flash system system.img
fastboot.exe -i 0x9886 reboot

pause
popd
